<?php
/* Template Name: 404 */
require 'variables.php';
get_header(); ?>

<div class="container mt-5 pt-5">
    <div class="row mt-5 pt-5 justify-content-center align-items-center align-content-center">
     Nothing here
    </div>
</div>

<?php get_footer(); ?>
