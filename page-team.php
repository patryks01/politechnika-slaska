<?php
/* Template Name: Strona drużyny */

require 'variables.php';
get_header('other'); ?>
<style>
  .team-link {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    flex-direction: column;
  }

  .team-position {
    color: #51aa74;
  }

  .team-position:hover {
    color: #51aa74;
  }



  .team-container {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    padding: 2rem 0;
    transition: 0.5s;
  }

  .team-container:hover {
    transform: scale(1.1);
  }

  @media (min-width: 1024px) {
    .team-text-container {
      margin-left: 2rem;
    }

    .team-link {
      flex-direction: row !important;
    }
  }
</style>
<div class="bg-grey">
    <div class="tlo position-relative container">
        <div class="container mt-5 container-new-page-title">
            <div class="tlo-img1 position-absolute w-75 h-75" style="background-image: url(/wp-content/themes/weblider/images/Group_187.svg) !important;">
            </div>
            <h2 class="new-page-title position-absolute"><?php echo get_the_title(); ?> <br> <span style="font-family: 'Zen Dots', cursive; font-weight:300!important;color:gray;">SAFE DNN</span></h2>
        </div>
    </div>
</div>
<div class="container mt-5">
  <div class="row mt-5">
    <?php $args = array(
      'post_type' => 'osoby',
      'order' => 'ASC'
    );

    $persons = new WP_Query($args);
    if ($persons->have_posts()) :
      foreach ($persons->posts as $p) { ?>

        <div class="col-md-6 d-flex justify-content-start align-items-center team-container">
          <a href="<?php echo get_the_permalink($p); ?>" class="w-100 team-link">
            <img class="img-fluid" src="<?php echo get_field('zdjecie_osoby', $p); ?>" alt="członek teamu">
            <div class="team-text-container d-flex flex-column justify-content-start align-items-start">
              <h3 class="team-name mt-3"><?php echo get_the_title($p) ?></h3>
              <p class="team-position"><?php echo get_field('stanowisko', $p) ?></p>
            </div>

          </a>
        </div>


    <?php }
    endif; ?>
  </div>
</div>
<?php get_footer(); ?>
