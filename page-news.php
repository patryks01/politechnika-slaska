<?php
/* Template Name: Aktualnosci */
require 'variables.php';
get_header('other'); ?>
<div class="bg-grey">
    <div class="tlo position-relative container">
        <div class="container mt-5 container-new-page-title">
            <div class="tlo-img1 position-absolute w-75 h-75" style="background-image: url(/wp-content/themes/weblider/images/Group_187.svg) !important;">
            </div>
            <h2 class="new-page-title position-absolute"><?php echo get_the_title(); ?> <br> <span style="font-family: 'Zen Dots', cursive; font-weight:300!important;color:gray;">SAFE DNN</span></h2>
        </div>
    </div>
</div>
<div class="container mt-5">
    <div class="row mt-5 justify-content-center align-items-center align-content-center news-container">
        <?php $i = 0;
        foreach ($news->posts as $new) { ?>
            <?php if ($i == 0) { ?>
                <div class="row pt-5 pb-5 main-card">
                    <a href="<?php echo get_the_permalink($new); ?>" class="col-lg-8 col-md-12 col-sm-12 firstpicture" style="background-image: url(<?php echo get_the_post_thumbnail_url($new); ?>);">
                    </a>
                    <div class="col-lg-3 col-md-12 col-sm-12 ml-lg-5">
                        <p class="mt-2"><?php echo get_the_time('j M Y', $new); ?></p>
                        <a href="<?php echo get_the_permalink($new); ?>">
                            <h2 class="my-4 testanim" style="position: relative"><?php echo get_the_title($new); ?></h2>
                        </a>
                        <p><?php echo get_the_excerpt($new); ?></p>
                        <?php if (pll_current_language() == 'pl') {  ?>
                            <a class="btn rounded-pill border border-light btntest my-4" href="<?php echo get_the_permalink($new); ?>">WIĘCEJ</a>
                        <?php }else{ ?>
                            <a class="btn rounded-pill border border-light btntest my-4" href="<?php echo get_the_permalink($new); ?>">READ MORE</a>
                        <?php } ?>
                    </div>
                </div>
                <?php $i++;
            } else { ?>
                <div class="row pt-md-5 pb-5 test-card">
                    <a href="<?php echo get_the_permalink($new); ?>" class="col-lg-5 col-md-12 col-sm-12 picturemobile" style="background-image: url(<?php echo get_the_post_thumbnail_url($new); ?>);">
                    </a>
                    <div class="col-lg-6 col-md-12 col-sm-12 ml-md-1 px-md-1 ml-lg-5">
                        <p class="mt-2 mt-md-0"><?php echo get_the_time('j M Y', $new); ?></p>
                        <a href="<?php echo get_the_permalink($new); ?>">
                            <h2 class="my-3"><?php echo get_the_title($new); ?></h2>
                        </a>
                        <p><?php echo get_the_excerpt($new); ?></p>
                        <?php if (pll_current_language() == 'pl') {  ?>
                            <a class="btn rounded-pill border border-light btntest my-4 mb-lg-0" href="<?php echo get_the_permalink($new); ?>">WIĘCEJ</a>
                        <?php }else{ ?>
                            <a class="btn rounded-pill border border-light btntest my-4 mb-lg-0" href="<?php echo get_the_permalink($new); ?>">READ MORE</a>
                        <?php } ?>
                    </div>
                </div>
            <?php }
        } ?>
    </div>
    <div class="row mb-5">
        <div style="visibility: hidden;" id="language"><?php echo pll_current_language(); ?></div>
        <?php  $countPag = ceil(count($allNews->posts) / 4) ?>
        <ul class="paginacja d-flex justify-content-center">
            <li><a id="pagination-link" class="pagination-publications-link" href="#">&lt;</a></li>
            <?php for($i=1;$i<=$countPag; $i++){ ?>
                <li><a id="pagination-link" class="pagination-news-link " href="#"><?php echo $i; ?></a></li>
            <?php } ?>
        </ul>
    </div>
</div>
<?php get_footer(); ?>
