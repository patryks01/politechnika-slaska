<?php
/* Template Name: Wyszukiwarka */
$q = $_GET['q'];
require 'variables.php';
get_header('other'); ?>
    <div class="bg-grey">
        <div class="tlo position-relative container">
            <div class="container mt-5 container-new-page-title">
                <div class="tlo-img1 position-absolute w-75 h-75" style="background-image: url(/wp-content/themes/weblider/images/Group_187.svg) !important;">
                </div>
                <h2 class="new-page-title position-absolute"><?php echo get_the_title(); ?> <br> <span style="font-family: 'Zen Dots', cursive; font-weight:300!important;color:gray;">SAFE DNN</span></h2>
            </div>
        </div>
    </div>
<div class="container mt-5">
    <div class="row mt-5 justify-content-center align-items-center align-content-center">
        <?php if ($results->posts) { ?>
            <?php $i = 0;
            foreach ($results->posts as $new) { ?>
                <?php if ($i == 0) { ?>
                    <div class="row pt-5 pb-5 main-card">
                        <a href="<?php echo get_the_permalink($new); ?>" class="col-lg-8 col-md-6 col-sm-12" style="border-radius: 1rem; height:550px;background-size: cover; background-image: url(<?php echo get_the_post_thumbnail_url($new); ?>);">
                        </a>
                        <div class="col-lg-3 col-md-6 col-sm-12 ml-lg-5">
                            <p><?php echo get_the_time('j M Y', $new); ?></p>
                            <a href="<?php echo get_the_permalink($new); ?>">
                                <h2 class="my-4"><?php echo get_the_title($new); ?></h2>
                            </a>
                            <p><?php echo get_the_excerpt($new); ?></p>
                            <a class="btn rounded-pill border border-light btntest my-4" href="<?php echo get_the_permalink($new); ?>">Więcej</a>
                        </div>
                    </div>
                <?php $i++;
                } else { ?>
                    <div class="row pt-md-5 pb-5 test-card">
                        <a href="<?php echo get_the_permalink($new); ?>" class="col-lg-5 col-md-12 col-sm-12 picturemobile" style="background-size: cover; background-image: url(<?php echo get_the_post_thumbnail_url($new); ?>);">
                        </a>
                        <div class="col-lg-6 col-md-12 col-sm-12 ml-md-1 px-md-1 ml-lg-5">
                            <p><?php echo get_the_time('j M Y', $new); ?></p>
                            <a href="<?php echo get_the_permalink($new); ?>">
                                <h2 class="my-2"><?php echo get_the_title($new); ?></h2>
                            </a>
                            <p><?php echo get_the_excerpt($new); ?></p>
                            <a class="btn rounded-pill border border-light btntest my-4 my-lg-3" href="<?php echo get_the_permalink($new); ?>">Więcej</a>
                        </div>
                    </div>
            <?php }
            } ?>
        <?php } else { ?>
        <?php if (pll_current_language() == 'pl') {  ?>
            <p>Nie znaleziono</p>
            <?php }else{ ?>
            <p>Nothing found</p>
            <?php } ?>
        <?php } ?>
    </div>
</div>
<?php get_footer(); ?>
