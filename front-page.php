<?php get_header(); ?>

<?php require 'variables.php'; ?>

<section id="upper-hero">
    <?php require 'sections/upper-hero.php'; ?>
</section>

<section id="bottom-hero">
    <?php require 'sections/bottom-hero.php'; ?>
</section>

<section id="content">
    <?php require 'sections/content.php'; ?>
</section>

<section id="news">
    <?php require 'sections/news.php'; ?>
</section>

<section id="team">
    <?php require 'sections/team.php'; ?>
</section>

<section id="publications">
    <?php require 'sections/publications.php'; ?>
</section>



<?php get_footer(); ?>