<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Politechnika Śląska</title>
    <meta name="description" content="Politechnika śląska - strona stworzona przez Weblider.eu">
    <meta name="author" content="Weblider.eu">
    <meta name="keywords" content="politechnika śląska, studia, śląsk">
    <link rel="stylesheet" href="/wp-content/themes/weblider/css/main.css">
    <link rel="stylesheet" href="/wp-content/themes/weblider/css/vendor/flickity.css">
    <link rel="icon" type="image/png" href="/wp-content/themes/weblider/images/favicon.png" />
</head>

<body>
<header class="HeaderWrapper">
    <div class="container fix ">



    <div class="HeaderWrapper__LogoWrapper">
        <?php if (pll_current_language() == 'pl') {  ?>
            <a href="/pl"><img src="/wp-content/themes/weblider/images/logo.svg" alt="Logo header" class="HeaderWrapper__Logo"></a>
        <?php } else { ?>
            <a href="/"><img src="/wp-content/themes/weblider/images/logo.svg" alt="Logo header" class="HeaderWrapper__Logo"></a>
        <?php } ?>
    </div>



    <div class="HeaderWrapper__FlexContainer">
        <div class="HeaderWrapper__InputContainer">
            <label for="search"></label>
            <?php if (pll_current_language() == 'pl') {  ?>
            <form action="/szukaj" method="GET">
                <?php }else{?>
                <form action="/search" method="GET">
                    <?php }?>
                    <input type="text" name="q"
                        <?php if (pll_current_language() == 'pl') {  ?>
                            placeholder="Szukaj"
                        <?php }else{ ?>
                            placeholder="Search"
                        <?php } ?>>
                    <button type="submit" class="border-0"><img src="/wp-content/themes/weblider/images/lupka.png" alt="lupka do wyszukiwarki"></button>
                </form>
        </div>
    </div>




    <nav class="HeaderWrapper__Navbar">
        <ul class="menu">
            <div class="HeaderWrapper__InputContainer">
                <label for="search"></label>
                <?php if (pll_current_language() == 'pl') {  ?>
                <form action="/szukaj" method="GET">
                    <?php }else{?>
                    <form action="/search" method="GET">
                        <?php }?>
                        <input type="text" name="q"
                            <?php if (pll_current_language() == 'pl') {  ?>
                                placeholder="Szukaj"
                            <?php }else{ ?>
                                placeholder="Search"
                            <?php } ?>>
                        <button type="submit" class="border-0"><img src="/wp-content/themes/weblider/images/lupka.png" alt="lupka do wyszukiwarki"></button>
                    </form>
            </div>
            <?php foreach (wp_get_nav_menu_items('menu ' . $lang = get_bloginfo("language")) as $item) {  ?>
                <li><a class="menu-item" href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a></li>
            <?php } ?>
        </ul>
    </nav>





    <div class="dropdown w-100 d-flex justify-content-center align-items-center align-content-center">
        <button onclick="myFunction()" class="dropbtn" aria-label="Change language"></button>
        <div id="myDropdown" class="dropdown-content">
            <a class="greenhov" href="<?php echo get_the_permalink(pll_get_post_translations($post->ID)['pl']) ?> ">
                <div class="flag-poland">
                    <img src="/wp-content/themes/weblider/images/poland.png" alt="">
                    <p class="pr-2 pl-2">PL</p>

                </div>
            </a>
            <a class="greenhov" href="<?php echo get_the_permalink(pll_get_post_translations($post->ID)['en']) ?> ">
                <div class="flag-english">
                    <img src="/wp-content/themes/weblider/images/united-kingdom.png" alt="">
                    <p class="pr-2 pl-2">EN</p>
                </div>
            </a>
        </div>
    </div>




    <div class="HeaderWrapper__Hamburger">
        <span class="fas fa-bars"></span>
        <span class="fas fa-times show"></span>
    </div>




    </div>
</header>
</body>

</html>
