<?php get_header('other'); ?>
<?php $post_type_obj = get_post_type_object( get_post_type($post));
echo $post_type_obj->labels->singular_name;
echo $post_type_obj->labels->name;?>
    <div class="bg-grey">
        <div class="tlo position-relative container">
            <div class="container mt-5 container-new-page-title">
                <div class="tlo-img1 position-absolute w-75 h-75" style="background-image: url(/wp-content/themes/weblider/images/Group_187.svg) !important;">
                </div>
                <h2 class="new-page-title position-absolute">
                    <?php if (pll_current_language() == 'pl') {  ?>
                        <?php echo $post_type_obj->labels->name; ?>
                    <?php }else{ ?>
                        <?php echo $post_type_obj->labels->singular_name; ?>
                    <?php } ?> <br> <span style="font-family: 'Zen Dots', cursive; font-weight:300!important;color:gray;">SAFE DNN</span></h2>
            </div>
        </div>
    </div>
    <div class="container my-5">
        <div class="row mt-5 ">
            <div class="col-lg-9 col-md-12 col-sm-12 padding-right-desktop justify-content-center align-items-center align-content-center">
                <h2 class="text-left pt-4 pb-4"><?php the_title(); ?></h2>
                <p class="mt-2" style="text-align: end; font-style:italic"><?php echo get_the_time('j M Y'); ?></p>
                <hr style="margin-left: 80%; width: 20%; height:2px; color:#51AA74; opacity:1; " />
                <img class="img-fluid" style="border-radius:3rem; width: auto; padding-top:1rem;" src="<?php echo get_the_post_thumbnail_url(); ?>" />
                <div class="row pt-5">
                    <p><?php the_content(); ?></p>
                </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12 padding-left-desktop ">
                <h3 class="text-left pt-4 pb-5 mb-4"><?php if (pll_current_language() == 'pl') {  ?> Czytaj więcej <?php }else{ ?> Read more <?php } ?></h3>
                <?php foreach (getOther(get_the_ID())->posts as $new) { ?>
                    <div class="row pt-2 pb-3 main-card flex-column">
                        <div class="col">
                            <a href="<?php echo get_the_permalink($new); ?>" class="w-100 firstpicture" style="height:200px;background-image: url(<?php echo get_the_post_thumbnail_url($new); ?>);">
                            </a>
                        </div>
                        <div class="col">
                            <a href="<?php echo get_the_permalink($new); ?>">
                                <h4 class="my-2 testanim" style="position: relative"><?php echo get_the_title($new); ?></h4>
                            </a>
                            <p style="font-size: 13px;line-height: 1.8"><?php echo get_the_excerpt($new); ?></p>
                            <?php if (pll_current_language() == 'pl') {  ?>
                                <a class="btn rounded-pill border border-light btntest my-2" href="<?php echo get_the_permalink($new); ?>">WIĘCEJ</a>
                            <?php }else{ ?>
                                <a class="btn rounded-pill border border-light btntest my-2" href="<?php echo get_the_permalink($new); ?>">READ MORE</a>
                            <?php } ?>
                        </div>
                    </div>

                <?php  } ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>
