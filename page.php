<?php get_header('other') ; ?>
<div class="bg-grey">
    <div class="tlo position-relative container">
        <div class="container mt-5 container-new-page-title">
            <div class="tlo-img1 position-absolute w-75 h-75" style="background-image: url(/wp-content/themes/weblider/images/Group_187.svg) !important;">
            </div>
            <h2 class="new-page-title position-absolute"><?php echo get_the_title(); ?> <br> <span style="font-family: 'Zen Dots', cursive; font-weight:300!important;color:gray;">SAFE DNN</span></h2>
        </div>
    </div>
</div>
<div class="container" style="padding:25px;">
    <?php the_content(); ?>
</div>

<?php get_footer(); ?>
