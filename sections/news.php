<div class="container">
    <div class="news-heading-container">
        <h2 class="news-heading alt-title"><?php echo wp_get_nav_menu_items('menu '.$lang=get_bloginfo("language"))[2]->title; ?> </h2>
    </div>
</div>
<div class="news-content">
    <div class="container">
        <div class="news-container row">
            <?php $i=1;foreach($news->posts as $new) { ?>
                <div class="news-body row col-sm-12 col-lg-12 col-xl-12 col-xxl-6 p-3">
                    <div class="news-body-img col-sm-12 col-md-12 col-lg-6 p-3 p-sm-1 p-md-2 mb-md-5">
                        <a href="<?php echo get_the_permalink($new); ?>" class="news-image" aria-label="Link to news page" style="margin: 0 auto; background-image: url(<?php echo get_the_post_thumbnail_url($new); ?>);">
                        </a>
                    </div>
                    <div class="news-body-text col-sm-12 col-md-12 col-lg-6 p-sm-1 p-md-2 mb-md-0">
                        <a href="<?php echo get_the_permalink($new); ?>"><h3 class="news-title text-light mb-3"><?php echo get_the_title($new); ?></h3></a>
                        <p class="news-text text-light"><?php echo get_the_excerpt($new); ?></p>
                        <?php if (pll_current_language() == 'pl') {  ?>
                            <a href="<?php echo get_the_permalink($new) ?>" class="btn btn-outline-light rounded-pill mt-3 mb-2 btn-all">WIĘCEJ</a>
                        <?php }else{ ?>
                            <a href="<?php echo get_the_permalink($new) ?>" class="btn btn-outline-light rounded-pill mt-3 mb-2 btn-all">READ MORE</a>
                        <?php } ?>
                    </div>
                </div>
                <?php if($i == 2){ ?>
                    <hr style="height:2px; color:white; opacity:1; margin: 0"/>
                <?php }?>
                <?php $i++;} ?>
        </div>
        <div class="row ">
            <?php if (pll_current_language() == 'pl') {  ?>
                <a href="/aktualnosci" class="btn btn-outline-light rounded-pill mb-4 btn-all"
                   style=""><p class="moretext">POZOSTAŁE AKTUALNOŚCI</p>
                </a>
            <?php }else{ ?>
                <a href="/news" class="btn btn-outline-light rounded-pill mb-4 btn-all"><p class="moretext">ALL NEWS</p>
                </a>
            <?php } ?>
        </div>
    </div>
</div>

