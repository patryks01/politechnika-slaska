<div class="container mt-5 mb-5 content-section">
    <div class="row">
        <div class="col-lg-5 col-md-6 col-sm-12 order-lg-0 order-first">
            <div id="div1" class="main-img">
                <img alt="pierwszy content" src="/wp-content/themes/weblider/images/Group_4.svg" />
            </div>
        </div>
        <div class="col-lg-7 col-md-6 col-sm-12 order-lg-1 ">
            <p>
                W ostatnich latach, zastosowanie głębokich sieci neuronowych poprawiło jakość działania wielu typów projektów informatycznych niezwiązanych ściśle z wymogami bezpieczeństwa funkcjonalnego.
                Głębokie sieci neuronowe cechują się umiejętnością generalizacji oraz operowania na podstawie nieznanych wcześniej danych wejściowych, dzięki czemu są stosowane jako potencjalne rozwiązania bardzo złożonych problemów, nawet tych, dla których obecnie rozwiązania analityczne nie są znane.</p>
        </div>
    </div>
    <div id="line"></div>
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12 order-lg-0">
            <p>
                Powyższe właściwości głębokich sieci neuronowych powodują, że ich potencjalne wykorzystanie staje się coraz bardziej popularne w wielu gałęziach przemysłu, w tym motoryzacyjnego.
                Wiele z tych systemów określane są jako krytyczne pod względem bezpieczeństwa, gdyż ich niepoprawne działanie może prowadzić do utraty zdrowia i życia ludzi.
                Potencjalne wykorzystanie głębokich sieci neuronowych w systemach krytycznych pod względem bezpieczeństwa wymaga uprzedniej systematycznej analizy niezawodności ich działania. </p>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12 order-lg-1 order-first">
            <div id="div2" class="main-img">
                <img alt="drugi content" src="/wp-content/themes/weblider/images/Group_1.svg" />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-5 col-md-6 col-sm-12 order-lg-0  order-first">
            <div id="div3" class="main-img">
                <img alt="trzeci content" src="/wp-content/themes/weblider/images/Group_5.svg" />
            </div>
        </div>
        <div class="col-lg-7 col-md-6 col-sm-12 order-lg-1 ">
            <p>
                Procedury do wykonania w ramach analizy głębokich sieci neuronowych nie są jednak zdefiniowane w opracowanych standardach opisujących metodyki tworzenia systemów krytycznych pod względem bezpieczeństwa, np. w dokumencie ISO 26262. Dodatkowo, standard IEC 61508 jawnie rekomenduje, aby nie używać w ogóle algorytmów uczenia maszynowego w takich systemach.</p>
        </div>
    </div>
    <div id="line2"></div>
    <div id="line3"></div>
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12 order-lg-0 order-md-0 order-sm-1">
            <p>
                W związku z powyższym, istniejące i znane procedury weryfikacji i walidacji systemów informatycznych nie mogą być wykorzystane do oceny niezawodności rozwiązań stosujących głębokie sieci neuronowe. Głównym celem niniejszego projektu jest opracowanie wymagań, które będą musiały być spełnione przez głębokie sieci neuronowe przed ich potencjalnym wykorzystaniem w systemach krytycznych pod względem bezpieczeństwa przemysłu motoryzacyjnego.</p>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12 order-lg-1 order-md-1 order-first">
            <div id="div4" class="main-img">
                <img alt="czwarty content" src="/wp-content/themes/weblider/images/Group_3.svg" />
            </div>
        </div>
    </div>
    <div id="line4"></div>
    <div class="row">
        <div class="col-lg-5 col-md-6 col-sm-12 order-lg-0  order-first">
            <div id="div5" class="main-img">
                <img alt="trzeci content" src="/wp-content/themes/weblider/images/Group_2.svg" />
            </div>
        </div>
        <div class="col-lg-7 col-md-6 col-sm-12 order-lg-1 ">
            <p>
                Procedury, które zostaną opracowane, będą zawierały zbiór metryk wraz z implementacją metod ich obliczania, które pozwolą na wymierną ocenę jakości i niezawodności głębokich sieci neuronowych.</p>
        </div>
    </div>
</div>