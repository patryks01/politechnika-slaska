<section id="team" class="TeamWrapper container">
    <div class="TeamWrapper__Slider row">
        <h2 class="TeamWrapper__TeamTitle alt-title"><?php echo wp_get_nav_menu_items('menu ' . $lang = get_bloginfo("language"))[3]->title; ?> </h2>
        <div class="mobile-image">

        </div>
        <div class="background-image">
            <?php require 'svg.php' ?>
        </div>
        <div class="main-carousel" data-flickity='{ "cellAlign": "center", "contain": true, "groupCells": true, "wrapAround":true, "pageDots": false, "freeScroll": false }'>
            <?php
            $args = array(
                'post_type' => 'osoby',
                'order' => 'ASC',
                'posts_per_page' => -1
            );
            $persons = new WP_Query($args);
            if ($persons->have_posts()) :
                foreach ($persons->posts as $p) { ?>
                    <div class="col-12 d-flex col-md-12  col-lg-6 col-xl-3 justify-content-center align-items-center flex-column">
                        <a href="<?php echo get_the_permalink($p); ?>" class="w-100 team-link">
                            <img class="img-fluid" src="<?php echo get_field('zdjecie_osoby', $p); ?>" alt="członek teamu">
                            <h3 class="team-name mt-3"><?php echo get_the_title($p) ?></h3>
                            <p class="team-position"><?php echo get_field('stanowisko', $p) ?></p>
                        </a>
                    </div>
            <?php }
            endif; ?>
        </div>
    </div>
    <?php if (pll_current_language() == 'pl') {  ?>
                <a href="/pl/zespol-2" class="btn btn-all-alt btn-outline-light rounded-pill mt-lg-5 mt-xl-0" style="z-index: 999999;">
                    <p class="moretext">Poznaj nasz zespół</p>
                </a>
            <?php } else { ?>
                <a href="/team" class="btn btn-all-alt btn-outline-light rounded-pill mt-lg-5 mt-xl-0"  style="z-index: 999999;">
                    <p class="moretext">Check our team</p>
                </a>
            <?php } ?>
</section>