<div class="container">
    <div class="publications-title-container">
        <h2 class="publications-title alt-title"><?php echo wp_get_nav_menu_items('menu ' . $lang = get_bloginfo("language"))[4]->title; ?></h2>
    </div>
</div>
<div class="publications-content" style="padding:7rem 0;">
    <div class="container publications-container">
        <div class="row">
            <div class="main-carousel" data-flickity='{ "cellAlign": "center", "contain": true, "groupCells": 1, "setGallerySize": false, "pageDots":false}'>
                <?php foreach ($publications->posts as $pub) { ?>
                    <div class="col-12 carousel-container">
                        <div class="col-md-6 publications-image-container mt-3">
                            <img class="img-fluid publications-image" src="<?php echo get_the_post_thumbnail_url($pub); ?>" alt="<?php echo get_the_title($pub); ?>">
                        </div>
                        <div class="col-md-6 p-sm-1 publications-text-container">
                            <div class="publications-header">
                                <h3 class="publications-subtitle">
                                    <?php echo get_the_title($pub); ?>
                                </h3>
                                <p class="publications-authors">
                                    Autorzy: Jan Nowak, Anna Nowak
                                </p>
                            </div>
                            <div class="publications-block-text">
                                <?php echo get_the_excerpt($pub); ?>
                            </div>
                            <?php if (pll_current_language() == 'pl') {  ?>
                                <a href="<?php echo get_the_permalink($pub); ?>" class="btn btn-more btn-outline-light rounded-pill mt-3 mb-2" style="padding: 10px 20px;">WIĘCEJ</a>
                            <?php } else { ?>
                                <a href="<?php echo get_the_permalink($pub); ?>" class="btn btn-more btn-outline-light rounded-pill mt-3 mb-2" style="padding: 10px 20px;">READ MORE</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <?php if (pll_current_language() == 'pl') {  ?>
                <a href="/publikacje/" class="btn btn-all btn-outline-light rounded-pill mt-sm-3 mt-xl-5">
                    <p class="moretext">POZOSTAŁE PUBLIKACJE</p>
                </a>
            <?php } else { ?>
                <a href="/publications" class="btn btn-all btn-outline-light rounded-pill mt-sm-3 mt-xl-5">
                    <p class="moretext">All publications</p>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
