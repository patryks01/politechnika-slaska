<?php if (is_home() || is_front_page()) { ?>
<div id="contact" class="container mt-5">
    <div class="row">
        <h2 class="contact-title alt-title"><?php echo wp_get_nav_menu_items('menu ' . $lang = get_bloginfo("language"))[5]->title; ?></h2>
    </div>
</div>
    <div id="map">
    </div>
<?php } ?>
<section class="contact-section pb-50 pt-50 ">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="contact-wrapper wow fadeInUp" data-wow-delay=".2s">
                    <?php if (pll_current_language() == 'pl') {  ?>
                        <form id="formularz" data-id="153" class="contact-form">
                    <?php } else { ?>
                        <form id="formularz" data-id="154" class="contact-form">
                    <?php } ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="single-form">
                                    <?php if (pll_current_language() == 'pl') {  ?>
                                        <input type="text" name="your-name" id="your-name" class="form-input" placeholder="Imię i nazwisko">
                                    <?php } else { ?>
                                        <input type="text" name="your-name" id="your-name" class="form-input" placeholder="Name">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-form">
                                    <?php if (pll_current_language() == 'pl') {  ?>
                                        <input type="email" name="your-email" id="your-email" class="form-input" placeholder="Adres E-mail">
                                    <?php } else { ?>
                                        <input type="email" name="your-email" id="your-email" class="form-input" placeholder="E-mail">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="single-form">
                                    <?php if (pll_current_language() == 'pl') {  ?>
                                        <input type="text" name="your-subject" id="your-subject" class="form-input" placeholder="Temat">
                                    <?php } else { ?>
                                        <input type="text" name="your-subject" id="your-subject" class="form-input" placeholder="Subject">
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="single-form">
                                    <?php if (pll_current_language() == 'pl') {  ?>
                                        <textarea name="your-message" id="your-message" class="form-input" rows="10" placeholder="Treść wiadomośći"></textarea>
                                    <?php } else { ?>
                                        <textarea name="your-message" id="your-message" class="form-input" rows="10" placeholder="Your message"></textarea>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="alert alert-success ml-2" id="contact-alert" role="alert">

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="submit-btn text-center">
                                    <?php if (pll_current_language() == 'pl') {  ?>
                                        <button class="main-btn btn btn-sumbit" id="send-mail" type="button">Wyślij</button>
                                    <?php } else { ?>
                                        <button class="main-btn btn btn-sumbit" id="send-mail" type="button">Submit</button>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="alert" id="alert-form" style="margin: 2rem auto; background: #51AA74; color:white;">ELO</div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-lg-5">
                <div>
                    <img src="/wp-content/themes/weblider/images/bialelogo.png" alt="">
                </div>
                <div>
                    <?php if (pll_current_language() == 'pl') {  ?>
                        <h3>Dane kontaktowe:</h3>
                        <p>Adres:</p>
                        <p>Telefony:</p>
                        <p>E-mail:</p>
                    <?php }else{?>
                        <h3>Contact details:</h3>
                        <p>Adress:</p>
                        <p>Telphone</p>
                        <p>E-mail:</p>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</section>
<footer class="footer">

    <div class="container">
        <div class="widget-wrapper">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="footer-widget">
                        <div class="logo">
                            <a href="/"> <img height="66" width="180" class="img-fluid" src="/wp-content/themes/weblider/images/logo.svg" alt="logo footer"> </a>
                        </div>
                        <div class="d-flex">
                            <div class="footer-widget d-flex ">
                                <ul class="links d-flex ">
                                    <?php $i=0;foreach (wp_get_nav_menu_items('footer ' . $lang = get_bloginfo("language")) as $item) {  ?>
                                            <?php if($i%2==0){ ?>
                                </ul>
                            </div>
                            <div class="footer-widget d-flex ">
                                <ul class="links d-flex ">
                                            <?php } ?>
                                        <li><a class="menu-item" href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a></li>
                                    <?php $i++;} ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">

        <div class="copy-right" style="border-top: 2px solid rgba(107, 111, 146, 0.44);">
            <div class="container">
                <div class="bottom-footer mb-3">
                <p>Copyright @ SafeDNN</p>
                    <div class="created-container">
                        <div class="d-flex flex-md-row">
                            <p style="padding-right: 0.5rem;">Created by</p><a href="http://weblider.eu/"><img class="weblider-logo" src="/wp-content/themes/weblider/images/logo-weblider.png" alt="Logo Weblider"></a>
                        </div>
                    </div>
                    <div class="social-container">
                        <a class="social-link" href="#">
                            <img class="social-icon" src="/wp-content/themes/weblider/images/facebook-icon.png" alt="">
                        </a>
                        <a class="social-link" href="#">
                            <img class="social-icon" src="/wp-content/themes/weblider/images/instagram-icon.png" alt="">
                        </a>
                        <a class="social-link" href="#">
                            <img class="social-icon" src="/wp-content/themes/weblider/images/twitter-icon.png" alt="">
                        </a>
                        <a class="social-link" href="#">
                            <img class="social-icon" src="/wp-content/themes/weblider/images/linked-in-icon.png" alt="">
                        </a>
                        <a class="social-link" href="#">
                            <img class="social-icon" src="/wp-content/themes/weblider/images/youtube-icon.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="copy-right">
            <div class="company-logos">
                <div class="company-logo">
                    <img src="/wp-content/themes/weblider/images/ncbir.png" alt="Logo NCBIR" />
                </div>
                <div class="company-logo">
                    <img src="/wp-content/themes/weblider/images/rzeczpospolita-polska.png" alt="Flaga Rzeczpospolitej Polski" />
                </div>
                <div class="company-logo">
                    <img src="/wp-content/themes/weblider/images/lider.svg" alt="Logo Lidera" />
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/gsap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/ScrollTrigger.min.js"></script>
<script>
    gsap.registerPlugin(ScrollTrigger);


if (window.location.pathname === '/') {
    const contentSection = document.querySelector('.content-section').children;
    const rightArrow = document.querySelectorAll('#arrow-right');
    const leftArrow = document.querySelectorAll('#arrow-left');
    const wireless1 = document.querySelectorAll('#wireless1');
    const wireless2 = document.querySelectorAll('#wireless2');

    function animate() {
        carAnimation();
        teamAnimation();
    }
    animate();
    setTimeout(animate, 4500);

    function teamAnimation() {
        for (let image of wireless1) {
            gsap.fromTo(wireless1, {
                opacity: 0,
            }, {
                duration: 1,
                opacity: 1,
                stagger: { // wrap advanced options in an object
                    each: 0.7,
                    from: "start",
                    ease: 'easeOut',
                },
                scrollTrigger: {
                    trigger: '#team',
                    start: 'top 40%',
                }
            })
        }

        for (let image of wireless2) {
            gsap.fromTo(wireless2, {
                opacity: 0,
            }, {
                duration: 1,
                opacity: 1,
                stagger: { // wrap advanced options in an object
                    each: 0.7,
                    from: "start",
                    ease: 'easeOut',
                },
                scrollTrigger: {
                    trigger: '#team',
                    start: 'top 40%',
                }
            })
        }
    }


    function carAnimation() {
        for (let arrow of rightArrow) {
            gsap.fromTo(rightArrow, {
                opacity: 0,
            }, {
                duration: 1,
                opacity: 1,
                stagger: { // wrap advanced options in an object
                    each: 1,
                    from: "start",
                    ease: 'easeInOut',
                },
                scrollTrigger: {
                    trigger: '#bottom-hero',
                    start: 'top 40%',
                }
            })
        }

        for (let arrow of leftArrow) {
            gsap.fromTo(leftArrow, {
                opacity: 0,
            }, {
                duration: 1,
                opacity: 1,
                stagger: { // wrap advanced options in an object
                    each: 1,
                    from: "end",
                    ease: 'easeInOut',
                },
                scrollTrigger: {
                    trigger: '#bottom-hero',
                    start: 'top 40%',
                }
            })
        }

        for (let contentElement of contentSection) {
        gsap.fromTo(contentElement, {
            opacity: 0,
        }, {
            duration: 1,
            opacity: 1,
            ease: 'easeInOut',
            scrollTrigger: {
                trigger: contentElement,
                start: 'top 80%',
            }
        })
    }

    gsap.fromTo('.bottom-heading-text', {
        x: '+=150',
        opacity: 0,
        scale: 1.2,
    }, {
        duration: 2,
        opacity: 1,
        scale: 1,
        x: '0',
        ease: 'easeInOut',
        scrollTrigger: {
            trigger: '.bottom-heading-text',
            start: 'top bottom',
        }

    })

    gsap.fromTo('#news', {
        y: '+=100',
        opacity: 0
    }, {
        y: 0,
        opacity: 1,
        stagger: 0.2,
        duration: 1,
        ease: 'easeInOut',
        scrollTrigger: {
            trigger: '#news',
            start: 'top 80%',
        }
    });
    }


}
    


    // gsap.fromTo('#arrow-right', {
    //     opacity: 0,
    // }, {
    //     opacity: 1,
    //     duration: 2,
    //     scrollTrigger: {
    //         trigger: '#bottom-hero',
    //         start: 'top 40%',
    //     }
    // })


  

  

    // gsap.fromTo('.bottom-hero-car', {
    //     x: '-=150',
    //     opacity: 0,
    //     scale: 1.2,
    // }, {
    //     duration: 2,
    //     opacity: 1,
    //     scale: 1,
    //     x: '0',
    //     ease: 'easeInOut',
    //     scrollTrigger: {
    //         trigger: '.bottom-hero-car',
    //         start: 'top 40%',
    //     }

    // })


    
</script>
<script
        src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
<script src="/wp-content/themes/weblider/js/vendor/flickity.pkgd.min.js"></script>
<script src="/wp-content/themes/weblider/js/jquery.js"></script>
<script src="/wp-content/themes/weblider/js/main.js"></script>
<script src="/wp-content/themes/weblider/js/hamburger.js"></script>
<script src="/wp-content/themes/weblider/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<?php 

if(is_front_page()) { ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB832_bHfRQLmwHuIxiTy8K-5_REW1AAlY&callback=initMap&q=Gliwice" async defer></script>
<?php } ?>
<script src="/wp-content/themes/weblider/js/map.js"></script>
<script src="/wp-content/themes/weblider/js/cookie.js"></script>
<div id="cookie-law-info-bar" class="w-100" style="z-index:9999; bottom: 0px; position: fixed; display: block;">
        <div class="w-100 d-flex ">
            <div class="align-self-center w-100" >
                <?php if (pll_current_language() == 'pl') { ?>
                    Korzystając z naszej strony, zgadzasz się na zapisywanie plików cookies.
                <?php }else{ ?>
                    By using our website, you consent to the storage of cookies.
                <?php } ?>
            </div>
            <div class="w-100" style="text-align: right; ">
                <?php if (pll_current_language() == 'pl') { ?>
                    <a class="polsl-href" href="/pl/polityka-prywatnosci/"> Więcej informacji o polityce prywatnośći </a>
                    <button type="button" id="accept_cookie" class="btn btn-success">Akceptuję </button>
                <?php }else{ ?>
                    <a class="polsl-href" href="/privacy-policy/"> More information about privacy policy </a>
                    <button type="button" id="accept_cookie" class="btn btn-success">Accept </button>
                <?php } ?>

            </div>
        </div>
</div>

<style>
    #cookie-law-info-bar{
        width: 100%;
        background-color: white;
        position: fixed;
        display: block;
        color: black;
        padding: 25px;
    }
    .btn-success{
        background-color: #51aa7d;
        padding: 7px 20px;
        box-shadow: 0px 3px 6px #0000004d;
        color: white;
    }
    .polsl-href{
        padding-left: 10px;
        padding-right: 10px;
    }
</style>
<script>
    $("#accept_cookie").on('click',function(){
        createCookie("visible_cookie", "true", 30);
        $("#cookie-law-info-bar").hide();

    })


</script>
