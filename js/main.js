
$("#contact-alert").hide();

function getFormData($form) {

    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}


function submitData(method, searchValue, url, fill, formId = null) {
    if(formId){
        var $form = $(formId);
        var serializedData = getFormData($form);
    }

    request = $.ajax({
        //uderzam do customowego routingu
        url: url,
        type: method,
        data: serializedData,
        fail: function () {},
        success: function (data) {
            //w data jest zwracany cały wygląd HTML wraz z wprowadzonymi danymi
            if(serializedData){
                $(fill).hide().fadeOut(250).html(data.message).fadeIn(350);
            }else{
                $(fill).hide().fadeOut(250).html(data).fadeIn(350);
            }
            //Więc wrzucam 'data' do tej klasy
        },
    });
}
$('#submit-data').on('click', function(event) {
    submitData('POST', null, '/wp-json/contact-form-7/v1/contact-forms/18/feedback', '#contact-alert', "#contact-form");
})

$('.pagination-news-link').on('click', function(event) {
    let language = $('#language').text();
    $('.pagination-news-link').removeClass('active');
    $(this).addClass('active')
    submitData('GET', this.text, '/wp-json/paginacja/v1/'+language+'/getNews/'+this.text, '.news-container');
})

$('.pagination-publications-link').on('click', function(event) {
    let language = $('#language').text();

    $('.pagination-publications-link').removeClass('active');
    $(this).addClass('active')
    submitData('GET', this.text, '/wp-json/paginacja/v1/'+language+'/getPublications/'+this.text, '.publications-container');
})


function adjustLine(from, to, line){

    var fT = from.offsetTop  + from.offsetHeight/2;
    var tT = to.offsetTop    + to.offsetHeight/2;
    var fL = from.offsetLeft + from.offsetWidth/2;
    var tL = to.offsetLeft   + to.offsetWidth/2;

    var CA   = Math.abs(tT - fT);
    var CO   = Math.abs(tL - fL);
    var H    = Math.sqrt(CA*CA + CO*CO);
    var ANG  = 180 / Math.PI * Math.acos( CA/H );

    if(tT > fT){
        var top  = (tT-fT)/2 + fT;
    }else{
        var top  = (fT-tT)/2 + tT;
    }
    if(tL > fL){
        var left = (tL-fL)/2 + fL;
    }else{
        var left = (fL-tL)/2 + tL;
    }

    if(( fT < tT && fL < tL) || ( tT < fT && tL < fL) || (fT > tT && fL > tL) || (tT > fT && tL > fL)){
        ANG *= -1;
    }
    top-= H/2;

    line.style["-webkit-transform"] = 'rotate('+ ANG +'deg)';
    line.style["-moz-transform"] = 'rotate('+ ANG +'deg)';
    line.style["-ms-transform"] = 'rotate('+ ANG +'deg)';
    line.style["-o-transform"] = 'rotate('+ ANG +'deg)';
    line.style["-transform"] = 'rotate('+ ANG +'deg)';
    line.style.top    = top+'px';
    line.style.left   = left+'px';
    line.style.height = H + 'px';
}

function doIT(){
    adjustLine(
        document.getElementById('div1'),
        document.getElementById('div2'),
        document.getElementById('line')
    );

    adjustLine(
        document.getElementById('div2'),
        document.getElementById('div3'),
        document.getElementById('line2')
    );
    adjustLine(
        document.getElementById('div3'),
        document.getElementById('div4'),
        document.getElementById('line3')
    );
    adjustLine(
        document.getElementById('div4'),
        document.getElementById('div5'),
        document.getElementById('line4')
    );
}

if ($('#line').length > 0) {
    setInterval(doIT,500);
  }




function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show-languages");
}

window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show-languages')) {
                openDropdown.classList.remove('show-languages');
            }
        }
    }
}

function getFormData($form)
{

  var unindexed_array = $form.serializeArray();
  var indexed_array = {};

  $.map(unindexed_array, function (n) {
    indexed_array[n['name']] = n['value'];
  });

  return indexed_array;
}

$('#alert-form').hide();

function submitContact(method, url, fill, formId) {
  var $form = $(formId);

  var serializedData = getFormData($form);
    request = $.ajax({
      //uderzam do customowego routingu
      url: url,
      type: method,
      data: serializedData,
      fail: function (data) {
        $(fill).hide().fadeOut(250).html(data.message).fadeIn(350);
      },
      success: function (data) {
        $(fill).hide().fadeOut(250).html(data.message).fadeIn(350);
      },
    });
}
$("#send-mail").click(function () {
  let method = "POST";
  let id = $("#formularz").data('id');
  let fill = "#alert-form";
  let url = "/wp-json/contact-form-7/v1/contact-forms/"+id+"/feedback";
  let searchValue = "";
  submitContact(method, url, fill, '#formularz');
});
